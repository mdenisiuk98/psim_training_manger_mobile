import 'package:training_manager_mobile/models/workout.dart';

class User{
  static final User _user = User._internal();
  String login;
  List<Workout> workouts;
  factory User(){
    return _user;
  }

  User._internal();

  void fromJson(dynamic json){
    _user.login=json['login'];
    List<dynamic> workoutsJson = json['workouts'];
    List<Workout> workoutsList = [];
    workoutsJson!=null ? workoutsJson.forEach((element)=>{
      workoutsList.add(Workout.fromJson(element))
    }) : null;
    _user.workouts=workoutsList;
  }

  void resetUser(){
    _user.login="";
    _user.workouts=[];
  }
}