import 'package:training_manager_mobile/models/exercise.dart';

class Workout{
  String name;
  List<Exercise> exercises;
  
  Workout(this.name, this.exercises);

  factory Workout.fromJson(dynamic json){
    List<Exercise> exercisesList = [];
    List<dynamic> exercisesJson = json['exercises'];
    exercisesJson!=null ? exercisesJson.forEach((element)=>{
      exercisesList.add(Exercise.fromJson(element))
    }) : null;
    return Workout(json['name'],exercisesList);
  }

}