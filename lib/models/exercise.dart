class Exercise {
  String id;
  int reps;
  double weight;

  Exercise(this.id,this.reps,this.weight);

  factory Exercise.fromJson(dynamic json){
    return Exercise(json['exercise'] as String, json['reps'] as int, json['weight'].toDouble() as double);
  }
}