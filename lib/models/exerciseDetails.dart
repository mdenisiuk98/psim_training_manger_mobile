class ExerciseDetails{
  String id;
  List<String> muscles;
  String name;

  ExerciseDetails(this.id,this.muscles,this.name);

  factory ExerciseDetails.fromJson(dynamic json){
    return ExerciseDetails(json['_id'] as String, List.from(json['muscles']), json['name'] as String);
  }
}