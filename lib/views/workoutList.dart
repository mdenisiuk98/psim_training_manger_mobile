import 'package:flutter/material.dart';
import 'package:training_manager_mobile/dataStorage/sessionStorage.dart';
import 'package:training_manager_mobile/models/user.dart';
import 'package:training_manager_mobile/models/workout.dart';
import 'package:training_manager_mobile/views/drawer.dart';
import 'package:training_manager_mobile/views/workingOut.dart';
import 'package:training_manager_mobile/views/workoutSummary.dart';

class WorkoutList extends StatelessWidget {
  const WorkoutList();

  @override
  Widget build(BuildContext context) {

    void showWorkoutHandler(Workout workout){
      Navigator.push(context,MaterialPageRoute(builder: (context) => WorkoutSummary(workout)));
    }

    void startWorkoutHandler(Workout workout){
      Navigator.push(context,MaterialPageRoute(builder: (context) => WorkingOut(workout)));
    }

    List<Widget> listItems = [];

    User user = User();
    user.workouts.forEach((element) => {
          listItems.add(
            ListTile(
              title: Text(element.name),
              trailing: SizedBox(
                width: 180.0,
                child: Row(
                  children: <Widget>[
                    MaterialButton(
                      color: Color(0xFFDC6E00),
                      child: Text("Show",style: Theme.of(context).textTheme.display2),
                      onPressed: ()=>showWorkoutHandler(element),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0),),
                    ),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 2.0),),
                    MaterialButton(
                      color: Color(0xFFC83737),
                      child: Text("Start",style: Theme.of(context).textTheme.display2),
                      onPressed: ()=>startWorkoutHandler(element),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0),),
                    )
                  ],
                ),
              ),
            )
          ),
          listItems.add(Divider())
        });

    return Container(
      child: Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          title: Text("My workouts"),
        ),
        resizeToAvoidBottomInset: false,
        body: Padding(
            padding: EdgeInsets.only(top: 30.0, left: 10.0, right: 10.0),
            child: ListView(
              children: listItems,
            )),
      ),
    );
  }
}
