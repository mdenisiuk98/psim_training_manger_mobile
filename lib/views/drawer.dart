import 'package:flutter/material.dart';
import 'package:training_manager_mobile/dataStorage/sessionStorage.dart';
import 'package:training_manager_mobile/helpers/fetch.dart';
import 'package:training_manager_mobile/models/user.dart';

class NavDrawer extends StatelessWidget {
  NavDrawer();

  @override
  Widget build(BuildContext context) {
    void logoutHandler() async {
      User user = User();
      user.resetUser();
      SessionStorage store = SessionStorage();
      await store.getSharedPreferncesInstance();
      Fetcher.fetchLogout(store.getSessionID());
      store.setSessionID("");
      store.setlogin("");
      Navigator.pushNamedAndRemoveUntil(
          context, "/login", (Route<dynamic> route) => false);
    }

    void myWorkoutsHandler() {
      Navigator.pushReplacementNamed(context, "/myWorkouts");
    }

    return Drawer(
      child: ListView(padding: EdgeInsets.only(top: 20.0), children: <Widget>[
        ListTile(
          title: Text(
            "My workouts",
            style: Theme.of(context).textTheme.body1,
          ),
          onTap: () => myWorkoutsHandler(),
        ),
        ListTile(
          title: Text("Log out", style: Theme.of(context).textTheme.body1),
          onTap: () => logoutHandler(),
        ),
      ]),
    );
  }
}
