import 'package:flutter/material.dart';
import 'package:training_manager_mobile/models/workout.dart';
import 'package:training_manager_mobile/views/drawer.dart';

class WorkoutDone extends StatelessWidget {
  final Workout workout;
  const WorkoutDone(this.workout);

  @override
  Widget build(BuildContext context) {
    void finishedWorkoutHandler() {
      Navigator.pop(context);
    }

    return Container(
        child: Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text("Working out ! (${workout.name})"),
      ),
      resizeToAvoidBottomInset: false,
      body: SizedBox.expand(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 30.0, bottom: 20.0),
              child: Text("Good job!",
                  style: Theme.of(context).textTheme.headline),
            ),
            Text("You finished your workout",
                style: Theme.of(context).textTheme.display4),
            Padding(
              child: Image.asset(
                "assets/img/biceps-min.png",
                colorBlendMode: BlendMode.color,
                height: 150.0,
                color: Theme.of(context).scaffoldBackgroundColor,
              ),
              padding: EdgeInsets.symmetric(vertical: 30.0),
            ),
            Text("Progress", style: Theme.of(context).textTheme.display1),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: SizedBox.fromSize(
                  size: Size(300.0, 30.0),
                  child: LinearProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                      value: 1.0,
                      backgroundColor: Colors.white70),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 40.0, bottom: 10.0),
              child: MaterialButton(
                shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0),),
                padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 40.0),
                color: Theme.of(context).buttonColor,
                onPressed: () => finishedWorkoutHandler(),
                child: Text("Done", style: Theme.of(context).textTheme.button),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
