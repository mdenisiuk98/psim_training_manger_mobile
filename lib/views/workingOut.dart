import 'package:flutter/material.dart';
import 'package:training_manager_mobile/dataStorage/exerciseStorage.dart';
import 'package:training_manager_mobile/models/exercise.dart';
import 'package:training_manager_mobile/models/workout.dart';
import 'package:training_manager_mobile/views/drawer.dart';
import 'package:training_manager_mobile/views/workoutDone.dart';

class WorkingOut extends StatefulWidget {
  final Workout workout;
  WorkingOut(this.workout);
  final ExerciseStorage exerciseStorage = ExerciseStorage();
  @override
  _WorkingOutState createState() => _WorkingOutState();
}

class _WorkingOutState extends State<WorkingOut> {
  int currentIndex = 0;
  void nextExerciseHandler() {
    if (currentIndex < widget.workout.exercises.length - 1) {
      setState(() {
        currentIndex += 1;
      });
    } else {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => WorkoutDone(widget.workout)));
    }
  }

  @override
  Widget build(BuildContext context) {
    Exercise currentExercise = widget.workout.exercises[currentIndex];
    Exercise nextExercise = (currentIndex < widget.workout.exercises.length - 1)
        ? widget.workout.exercises[currentIndex + 1]
        : null;

    return Container(
      child: Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          title: Text("Working out ! (${widget.workout.name})"),
        ),
        resizeToAvoidBottomInset: false,
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 0.0, top: 30.0),
              child: Text(
                  widget.exerciseStorage.getExercise(currentExercise.id).name,
                  style: Theme.of(context).textTheme.headline),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(currentExercise.reps.toString() + " reps",
                      style: Theme.of(context).textTheme.display4),
                  Text(
                      currentExercise.weight.toStringAsPrecision(2) +
                          " kg weight",
                      style: Theme.of(context).textTheme.display4),
                ],
              ),
            ),
            MaterialButton(
              padding: EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 40.0,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(35.0)),
              color: Theme.of(context).buttonColor,
              onPressed: () => nextExerciseHandler(),
              child: Text("Next!", style: Theme.of(context).textTheme.button),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15.0),
                color: Color(0xFF313131),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: Text(
                        "Keep going!\nYou're doing great!",
                        style: Theme.of(context).textTheme.display2,
                        textWidthBasis: TextWidthBasis.longestLine,
                      ),
                    ),
                    Spacer(),
                    Image.asset(
                      "assets/img/biceps-min.png",
                      colorBlendMode: BlendMode.color,
                      height: 80.0,
                      color: Color(0xFF313131),
                    )
                  ],
                ),
              ),
            ),
            Text("Progress", style: Theme.of(context).textTheme.display1),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: SizedBox.fromSize(
                  size: Size(300.0, 30.0),
                  child: LinearProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                      value: currentIndex.toDouble() /
                          widget.workout.exercises.length.toDouble(),
                      backgroundColor: Colors.white70),
                ),
              ),
            ),
            Text("Up next", style: Theme.of(context).textTheme.display1),
            (nextExercise != null)
                ? Padding(
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Color(0xFF373636),),
                      child: ListTile(
                        title: Text(
                          widget.exerciseStorage
                              .getExercise(nextExercise.id)
                              .name,
                          style: Theme.of(context).textTheme.display1,
                        ),
                        subtitle: Padding( child: widget.exerciseStorage
                                    .getExercise(nextExercise.id)
                                    .name !=
                                "Rest"
                            ? Text(
                                nextExercise.reps.toStringAsFixed(0) +
                                    " reps,  " +
                                    nextExercise.reps.toStringAsFixed(0) +
                                    " kg weight",
                                style: Theme.of(context).textTheme.display3,
                              )
                            : Text(
                                nextExercise.reps.toStringAsFixed(0) + " min",
                                style: Theme.of(context).textTheme.display3,
                              ), padding: EdgeInsets.only(top: 15.0),),
                      ),
                      
                    ),
                    padding:
                        EdgeInsets.only(left: 60.0, right: 60.0, top: 20.0),
                  )
                : Padding(child: Text("You're done!"), padding: EdgeInsets.only(top: 20.0),),
          ],
        ),
      ),
    );
  }
}
