import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:training_manager_mobile/dataStorage/exerciseStorage.dart';
import 'package:training_manager_mobile/dataStorage/sessionStorage.dart';
import 'package:training_manager_mobile/helpers/fetch.dart';
import 'package:training_manager_mobile/models/user.dart';
import 'package:training_manager_mobile/views/drawer.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen();

  @override
  Widget build(BuildContext context) {
    handleContinuePress() {
      Navigator.pushNamed(context, "/myWorkouts");
    }

    void setupExerciseList() async {
      ExerciseStorage().loadExercisesMap(await Fetcher.fetchExercises());
    }

    setupExerciseList();

    return Container(
      child: Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          title: Text("Training Manager"),
        ),
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: EdgeInsets.only(top: 30.0, left: 60.0, right: 60.0),
          child: SizedBox.expand(
            child: Column(
              children: <Widget>[
                SvgPicture.asset("assets/img/logo-min.svg", height: 200.0),
                Padding(
                    child: Text("Welcome back, ${User().login}!",
                    textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.display1),
                    padding: EdgeInsets.only(top: 50.0)),
                Padding(
                  child: MaterialButton(
                    child: Text("Continue",
                        style: Theme.of(context).textTheme.button),
                    color: Theme.of(context).buttonColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0)),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
                    onPressed: () => handleContinuePress(),
                  ),
                  padding: EdgeInsets.only(top: 100.0),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
