import 'package:flutter/material.dart';
import 'package:training_manager_mobile/dataStorage/exerciseStorage.dart';
import 'package:training_manager_mobile/models/exerciseDetails.dart';
import 'package:training_manager_mobile/models/workout.dart';
import 'package:training_manager_mobile/views/drawer.dart';
import 'package:training_manager_mobile/views/workingOut.dart';

class WorkoutSummary extends StatelessWidget {
  final Workout workout;
  const WorkoutSummary(this.workout);

  @override
  Widget build(BuildContext context) {
    void startWorkoutHandler(Workout workout) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => WorkingOut(workout)));
    }

    String musclesSummary = "";
    int predictedMinutes = 0;
    Map<String, double> muscleStrain = {};
    ExerciseStorage details = ExerciseStorage();
    List<Widget> exerciseTiles = [];
    double fullStrain = 0.0;
    workout.exercises.forEach((element) => {
          predictedMinutes += element.reps * 10,
          details.exercises[element.id].muscles.forEach((muscle) => {
                if (muscleStrain.containsKey(muscle))
                  {
                    muscleStrain[muscle] +=
                        element.reps.toDouble() * element.weight
                  }
                else
                  {
                    muscleStrain[muscle] =
                        element.reps.toDouble() * element.weight
                  },
                fullStrain += element.reps.toDouble() * element.weight
              }),
          exerciseTiles.add(
            Container(
              padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 1.0),
              child: Container(
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Color(0xFF373636),),
                padding: EdgeInsets.symmetric(vertical: 15.0),
                child: ListTile(
                  title: Text(
                    details.exercises[element.id].name,
                    style: Theme.of(context).textTheme.display1,
                  ),
                  subtitle: Padding(
                      child: details.exercises[element.id].name != "Rest"
                          ? Text(
                              element.reps.toStringAsFixed(0) +
                                  " reps,  " +
                                  element.reps.toStringAsFixed(0) +
                                  " kg weight",
                              style: Theme.of(context).textTheme.display3,
                            )
                          : Text(
                              element.reps.toStringAsFixed(0) + " min",
                              style: Theme.of(context).textTheme.display3,
                            ),
                      padding: EdgeInsets.only(top: 25.0)),
                ),
              ),
            ),
          )
        });
    muscleStrain.forEach((muscle, strain) => {
          musclesSummary +=
              (muscle + " (" + (strain / fullStrain * 100.0).toString() + "%)")
        });
    predictedMinutes = predictedMinutes ~/ 60;
    return Container(
      child: Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          title: Text(workout.name),
        ),
        resizeToAvoidBottomInset: false,
        body: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: 160.0,
              child: Container(
                color: Color(0xFF313131),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Text("Summary",
                          style: Theme.of(context).textTheme.body1),
                    ),
                    Text("Muscles worked out:",
                        style: Theme.of(context).textTheme.display2),
                    Text(
                      musclesSummary,
                      style: Theme.of(context).textTheme.display2,
                      textAlign: TextAlign.center,
                    ),
                    Text("Estimated time:",
                        style: Theme.of(context).textTheme.display2),
                    Text(predictedMinutes.toStringAsFixed(0) + " min",
                        style: Theme.of(context).textTheme.display2),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child:
                  Text("Exercises", style: Theme.of(context).textTheme.body1),
            ),
            Expanded(
              flex: 1,
              child: ListView(
                children: exerciseTiles,
              ),
            ),
            Padding(
                padding: EdgeInsets.only(bottom: 40.0, top: 20.0),
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  padding: EdgeInsets.all(14.0),
                  color: Theme.of(context).buttonColor,
                  child: Text("Start now"),
                  onPressed: () => startWorkoutHandler(workout),
                )),
          ],
        ),
      ),
    );
  }
}
