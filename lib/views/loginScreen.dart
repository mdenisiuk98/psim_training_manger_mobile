import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:training_manager_mobile/dataStorage/sessionStorage.dart';
import 'package:training_manager_mobile/helpers/fetch.dart';
import 'package:training_manager_mobile/models/user.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController loginController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool loginFail = false;
  bool initialLoading = true;

  Future<void> handleLoginPress() async {
    setState(() {
      loginFail = false;
    });

    String sessionID = await Fetcher.fetchSessionID(
        loginController.text, passwordController.text);

    if (sessionID != null && sessionID != "") {
      SessionStorage store = SessionStorage();
      await store.getSharedPreferncesInstance();
      await store.setSessionID(sessionID);
      User user = await Fetcher.fetchUserData(sessionID);
      if (user.login != "" && user.login != null) {
        await store.setlogin(user.login);
      }
      Navigator.pushReplacementNamed(context, '/welcome');
    } else {
      setState(() {
        loginFail = true;
      });
    }
  }

  Future<void> autoLoginCheck() async {
    SessionStorage store = new SessionStorage();
    bool shouldShowLoginForm = false;

    await store.getSharedPreferncesInstance();
    String sessionID = store.getSessionID();
    String login = store.getLogin();
    if (sessionID != null && sessionID != "") {
      bool validSession = await Fetcher.fetchSessionVaidation(login, sessionID);
      if (validSession) {
        User user = await Fetcher.fetchUserData(sessionID);
        if (user.login != "" && user.login != null) {
          Navigator.pushReplacementNamed(context, "/welcome");
        } else {
          shouldShowLoginForm = true;
        }
      } else {
        shouldShowLoginForm = true;
      }
    } else {
      shouldShowLoginForm = true;
    }
    if (shouldShowLoginForm) {
      setState(() {
        initialLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    autoLoginCheck();

    final Column loginForm = Column(
      children: <Widget>[
        SvgPicture.asset("assets/img/logo-min.svg", height: 200.0),
        if (loginFail)
          Padding(
              child: Text(
                  "The server didn't respond or your login information was invalid!", style: Theme.of(context).textTheme.caption),
              padding: EdgeInsets.only(top: 20.0),),
        Padding(
            child: Text(
              "Username",
              style: Theme.of(context).textTheme.display1,
            ),
            padding: EdgeInsets.only(top: 30.0)),
        TextField(
          controller: loginController,
          style: Theme.of(context).textTheme.body2,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 0.0),
            fillColor: Color(0xFFD2D2D2),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(40.0),
            ),
          ),
        ),
        Padding(
            child: Text(
              "Password",
              style: Theme.of(context).textTheme.display1,
            ),
            padding: EdgeInsets.only(top: 20.0)),
        TextField(
            controller: passwordController,
            obscureText: true,
            style: Theme.of(context).textTheme.body2,
             decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 0.0),
            fillColor: Color(0xFFD2D2D2),
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(40.0),
            ),
          ),),
            
        Padding(
          child: MaterialButton(
            color: Theme.of(context).buttonColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0)),
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
            child: Text("Log in", style: Theme.of(context).textTheme.button),
            onPressed: () => handleLoginPress(),
          ),
          padding: EdgeInsets.only(top: 40.0),
        ),
      ],
    );
    final Widget loading = Text("Loading");

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Training Manager"),
        ),
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: EdgeInsets.only(top: 30.0, left: 60.0, right: 60.0),
          child: SizedBox.expand(
            child: initialLoading ? loading : loginForm,
          ),
        ),
      ),
    );
  }
}
