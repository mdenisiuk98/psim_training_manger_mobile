import 'package:flutter/material.dart';
import 'package:training_manager_mobile/theme/style.dart';
import 'package:training_manager_mobile/views/loginScreen.dart';
import 'package:training_manager_mobile/views/welcomeScreen.dart';
import 'package:training_manager_mobile/views/workoutList.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Training Manager',
      theme: defaultTheme,
      initialRoute: "/login",
      routes: <String, WidgetBuilder>{
        "/login": (context)=> LoginScreen(),
        "/welcome": (context)=> WelcomeScreen(),
        "/myWorkouts": (context)=> WorkoutList(),
      },
    );
  }
}
