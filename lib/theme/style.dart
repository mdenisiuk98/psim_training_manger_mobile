import 'package:flutter/material.dart';

final ThemeData defaultTheme = ThemeData(
  brightness: Brightness.dark,
  primaryColor: Color(0xFF1E1E1E),
  scaffoldBackgroundColor: Color(0xFF292929),
  buttonColor: Color(0xFFC83737),
  accentColor: Color(0xFFDC6E00),
  fontFamily: "Roboto",
  textTheme: TextTheme(
    headline: TextStyle(
      fontFamily: "Roboto",
      fontSize: 30.0,
      fontWeight: FontWeight.bold,
      color: Color(0xFFD2D2D2),
      letterSpacing: 2.0,
    ),
    body1: TextStyle(
      fontFamily: "Roboto",
      fontSize: 24.0,
      fontWeight: FontWeight.normal,
      color: Color(0xFFD2D2D2),
      letterSpacing: 3.0,
    ),
    button: TextStyle(
      fontFamily: "Roboto",
      fontSize: 24.0,
      fontWeight: FontWeight.w400,
      color: Color(0xFFD2D2D2),
      letterSpacing: 2.0,
    ),
    body2: TextStyle(
      fontFamily: "Roboto",
      fontSize: 20.0,
      fontWeight: FontWeight.normal,
      color: Color(0xFF1E1E1E),
      letterSpacing: 2.0,
     ),
    caption: TextStyle(
      fontFamily: "Roboto",
      fontSize: 18.0,
      fontWeight: FontWeight.w300,
      color: Color(0xFFD2D2D2),
     ),
     display1: TextStyle(
      fontFamily: "Roboto",
      fontSize: 20.0,
      fontWeight: FontWeight.bold,
      color: Color(0xFFD2D2D2),
      letterSpacing: 2.0,
     ),
     display2: TextStyle(
      fontFamily: "Roboto",
      fontSize: 16.0,
      fontWeight: FontWeight.w400,
      color: Color(0xFFD2D2D2),
      letterSpacing: 2.0,
     ),
      display3: TextStyle(
      fontFamily: "Roboto",
      fontSize: 14.0,
      fontWeight: FontWeight.w400,
      color: Color(0xFF9B9B9B),
      letterSpacing: 2.0,
     ),
       display4: TextStyle(
      fontFamily: "Roboto",
      fontSize: 22.0,
      fontWeight: FontWeight.w400,
      color: Color(0xFF9B9B9B),
      letterSpacing: 2.0,
     )
  ),
  iconTheme: IconThemeData(size: 350.0),
);

final BoxDecoration backgroundGradient = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFFFFF2DC),
      Color(0xFFFFEEFF),
    ],
    begin: Alignment(0.0,-0.85),
    end: Alignment.bottomCenter,
  ),
);
