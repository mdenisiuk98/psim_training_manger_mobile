import 'dart:convert';

import 'package:training_manager_mobile/models/exerciseDetails.dart';
import 'package:training_manager_mobile/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:training_manager_mobile/models/workout.dart';
class Fetcher{
  static Future<String> fetchSessionID(String login, String password) async {
    Map<String, dynamic> request = {
      "login": login,
      "password": password
    };
    dynamic reqBody = jsonEncode(request);
    final response = await http.post("https://training-manager-psim.herokuapp.com/auth/login", body: reqBody, headers:{"Content-Type": "application/json","Accept": "application/json"});
    if(response.statusCode==200){
    dynamic parsed = jsonDecode(response.body);
      return parsed['session'];
    }
    else{
      return "";
    }
  }

  static Future<bool> fetchSessionVaidation(String login, String sessionID) async{
      Map<String, dynamic> request = {
      "login": login,
      "session": sessionID
    };
    dynamic reqBody = jsonEncode(request);
    final response = await http.post("https://training-manager-psim.herokuapp.com/auth/verifySession", body: reqBody, headers:{"Content-Type": "application/json","Accept": "application/json"});
    if(response.statusCode==200){
      dynamic success = jsonDecode(response.body)['success'];
      return ((success is bool) ? success : false);
    }
    else{
      return false;
    }
  }
  static Future<User> fetchUserData(String sessionID) async{
    User user = User();
     Map<String, dynamic> request = {
      "sessionID": sessionID,
    };
    dynamic reqBody = jsonEncode(request);
    final response = await http.post("https://training-manager-psim.herokuapp.com/api/userData", body: reqBody, headers:{"Content-Type": "application/json","Accept": "application/json"});
    if(response.statusCode==200){
      dynamic parsed = jsonDecode(response.body);
      user.fromJson(parsed['user']);
    }
    else{
      user.resetUser();
    }
    return user;
  }


  static Future<Map<String,ExerciseDetails>> fetchExercises() async{
    List<ExerciseDetails> exercises = [];
    Map<String,ExerciseDetails> exerciseMap = {};
    final response = await http.get("https://training-manager-psim.herokuapp.com/api/exercisesData",headers:{"Content-Type": "application/json","Accept": "application/json"});
    if(response.statusCode==200){
      dynamic parsed = jsonDecode(response.body);
      parsed['exercises']!=null ? parsed['exercises'].forEach((element)=>{
        exercises.add(ExerciseDetails.fromJson(element))
      }) : null;
      exercises.forEach((element)=>{
        exerciseMap.putIfAbsent(element.id, ()=>element)
      });
    }
    return exerciseMap;
  }


  static void fetchLogout(sessionID){
     Map<String, dynamic> request = {
      "session": sessionID
    };
    dynamic reqBody = jsonEncode(request);
    http.post("https://training-manager-psim.herokuapp.com/auth/logout",body: reqBody, headers:{"Content-Type": "application/json","Accept": "application/json"});
  }
}