import 'package:shared_preferences/shared_preferences.dart';

class SessionStorage{
  static final SessionStorage _instance = SessionStorage._internal();
  SharedPreferences localStorage;
  factory SessionStorage(){
    return _instance;
  }
  void getSharedPreferncesInstance()async{
    _instance.localStorage = await SharedPreferences.getInstance();
  }
  void setSessionID(String sessionID) async{
    await _instance.localStorage.setString("sessionID", sessionID);
  }
  String getSessionID(){
    return _instance.localStorage.getString("sessionID");
  }
  void setlogin(String login)async{
   await  _instance.localStorage.setString("login", login);
  }
  String getLogin(){
    return _instance.localStorage.getString("login");
  }
  SessionStorage._internal();
}