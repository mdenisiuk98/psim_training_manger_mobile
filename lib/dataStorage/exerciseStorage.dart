import 'package:training_manager_mobile/models/exerciseDetails.dart';

class ExerciseStorage{
  static final ExerciseStorage _instance = ExerciseStorage._internal();
  Map<String, ExerciseDetails> exercises = {};

  factory ExerciseStorage(){
    return _instance;
  }

  ExerciseStorage._internal();

  void loadExercisesMap(Map<String,ExerciseDetails> map){
    _instance.exercises = map;
  }

  ExerciseDetails getExercise(String id){
    return _instance.exercises[id];
  }
}